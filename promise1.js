module.exports = function getBoard(boardsData, boardId, cb) {
    if (arguments.length !== 2) throw new Error("Some parameter is missing");
      if (!Array.isArray(boardsData))
        throw new Error("Board data is not an array");
  
    return new Promise(function(resolve,reject){
      
      setTimeout(() => {
        let data = boardsData.find((element) => element.id === boardId);
        if (data) {
        resolve(data)
        } else {
          reject("err");
        }
      }, 2 * 1000);
    }
    ) 
}
  