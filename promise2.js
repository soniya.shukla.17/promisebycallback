module.exports = function getList(listData, boardId, cb) {
    
      if (arguments.length !== 2) throw new Error("Some parameter is missing");
    
      if(typeof listData !== 'object')
        throw new Error("list data is not a object")
       return new Promise(function(resolve,reject){
      setTimeout(() => {
        
        if (listData.hasOwnProperty(boardId)) {
            
        resolve(listData[boardId]);
        } else {
          reject("id not found");
        }
      }, 2 * 1000);
    } )
  };
  