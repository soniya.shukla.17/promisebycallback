module.exports = function getCards(cardsData,listId) {
    
      if (arguments.length !== 2) 
      {
        throw new Error("Some parameter is missing");
      }
  
      if(typeof cardsData !== 'object')
      {
        throw new Error("card data is not a object")
      }
        return new Promise(function(resolve,reject){
      setTimeout(() => {
        let data = cardsData.hasOwnProperty(listId)
        if (data) {    
          resolve(cardsData[listId]);
        } else {
          reject("id not found",);
        }
      }, 2 * 1000);
    } )
  };
  