module.exports = function thanosInformation(allInformation) {
  return new Promise((resolve,reject) => {

  let thanosObject = allInformation.boardsData.find(
    (element) => element.name === allInformation.boardName
  );
  //console.log(thanosObject);
  if (arguments.length !== 1) reject("Some parameter is missing");
  if (!Array.isArray(allInformation.boardsData))
    reject("Board data is not an array");
  if (typeof allInformation.listData !== "object")
    reject("list data is not a object");
  setTimeout(() => {
    allInformation.getBoard(allInformation.boardsData, thanosObject.id)
    .then((data) => {
      console.log(data);
      return allInformation.getList(allInformation.listData, data.id);
    })
    .then((data) => {
      console.log(data);
      let mindId = data.find((element) => element.name === "Mind");
      return allInformation.getCard(
            allInformation.cardsData,
            mindId.id)
    })
    .then((data) => {
      resolve(data);
    })
    .catch((err) => {
      reject(err);
    })
  }, 2 * 1000);
})
};
