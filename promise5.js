module.exports = function thanosInformation(allInformation) {
  
  let thanosObject = allInformation.boardsData.find(
    (element) => element.name === allInformation.boardName
  );
 // console.log(thanosObject);
  if (arguments.length !== 1) throw new Error("Some parameter is missing");
  if (!Array.isArray(allInformation.boardsData))
    throw new Error("Board data is not an array");
  if (typeof allInformation.listData !== "object")
    throw new Error("list data is not a object");
  return new Promise((resolve,reject) => {
    setTimeout(() => {
      allInformation
        .getBoard(allInformation.boardsData, thanosObject.id)
        .then((data) => {
          console.log(data);
          return allInformation.getList(allInformation.listData, data.id);
        })
        .then((data) => {
          console.log(data);
          let mindId = data.filter(
            (element) => element.name === "Mind" || element.name === "Space"
          );
          let arr=[]
          mindId
            .forEach((obj) => {
              arr.push(allInformation.getCard(allInformation.cardsData, obj.id));
            })    
          return  arr 
        })
        .then((data)=>{
            resolve(data)
        })
        .catch((err) => {
            reject(err);
          })
        }, 2 * 1000);
      })
    
}
          