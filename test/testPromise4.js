let boardsData = require("./../boards.json")
let cardsData = require("./../cards.json")
let listData = require("./../lists.json")

thanosInformation = require("./../promise4")

getBoard = require("./../promise1")

getList = require("./../promise2")

getCard = require("./../promise3")

let allInformation={
    "boardsData" : boardsData,
    "listData" : listData,
    "cardsData" : cardsData,
    "getBoard" : getBoard,
    "getList": getList,
    "getCard" : getCard,
    "boardName": "Thanos"

}


thanosInformation(allInformation)
.then(data=>{
    let offset = 0;
    data.forEach((element) => {
        setTimeout(() => {
          console.log(element);
        }, 2000 + offset);
        offset += 1000;
      });
})
.catch(reject=>{
    console.log(reject)
})
